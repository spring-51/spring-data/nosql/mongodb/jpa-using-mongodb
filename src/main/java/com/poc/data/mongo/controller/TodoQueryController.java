package com.poc.data.mongo.controller;

import com.poc.data.mongo.enums.StatusLookupEnum;
import com.poc.data.mongo.model.TodoDto;
import com.poc.data.mongo.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;


@RestController
@RequestMapping("/query")
public class TodoQueryController {
    @Autowired
    private final TodoRepository todoRepo;

    public TodoQueryController(TodoRepository todoRepo) {
        this.todoRepo = todoRepo;
    }

    @GetMapping("/todos")
    public ResponseEntity<?> getAllTodos(){
        List<TodoDto> todos =  todoRepo.findAllTodos(
                StatusLookupEnum.ACTIVE.name(),
                StatusLookupEnum.ACTIVE.getId());
        if(ObjectUtils.isEmpty(todos)){
            todos = new ArrayList<>();
        }
        return ok(todos);
    }

    @GetMapping("/todos/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") String id){
        TodoDto todo =  todoRepo.findTodoById(id)
                .orElse(new TodoDto());
        return ok().body(todo);
    }

    @GetMapping("/todos/like/case-sensitive")
    public ResponseEntity<?> findTodoByLikeTodoStrCaseSensitive(
            @RequestParam(name = "str",required = false, defaultValue = "test") String str){
        List<TodoDto> todo =  todoRepo.findTodoByLikeTodoStrCaseSensitive(str)
                .orElse(new ArrayList<>());
        return ok().body(todo);
    }

    // refer - https://www.tutorialspoint.com/mongodb-regex-search-on-integer-value
    @GetMapping("/todos/like/integer-and-string/case-sensitive")
    public ResponseEntity<?> findTodoByLikeTodoStrCaseSensitive(
            @RequestParam(name = "field",required = false, defaultValue = "todoId") String field,
            @RequestParam(name = "searchStr",required = false, defaultValue = "00") String searchStr){
        List<String> allowedFields = Arrays.asList("todo", "todoId");
        if(!allowedFields.contains(field)){
            throw new RuntimeException("invalid 'field' value, allowed values are - "+allowedFields.toString());
        }
        List<TodoDto> todo =  todoRepo.findTodoByLikeTodoStrAndIntegerCaseSensitive(field, searchStr)
                .orElse(new ArrayList<>());
        return ok().body(todo);
    }

    @GetMapping("/todos/like/case-insensitive")
    public ResponseEntity<?> findTodoByLikeTodoStrCaseInsensitive(
            @RequestParam(name = "str", required = false, defaultValue = "test") String str){
        List<TodoDto> todo =  todoRepo.findTodoByLikeTodoStrCaseInsensitive(str)
                .orElse(new ArrayList<>());
        return ok().body(todo);
    }

    @GetMapping("/todos/in")
    public ResponseEntity<?> findTodoByInIds(
            @RequestParam(name = "ids", required = false, defaultValue = "") List<String> ids){
        List<TodoDto> todo =  todoRepo.findTodoByInIds(ids)
                .orElse(new ArrayList<>());
        return ok().body(todo);
    }
}
