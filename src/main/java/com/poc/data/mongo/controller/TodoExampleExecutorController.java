package com.poc.data.mongo.controller;

import com.poc.data.mongo.model.TodoDto;
import com.poc.data.mongo.repository.TodoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = "/example-executor")
@RequiredArgsConstructor
public class TodoExampleExecutorController {

    private final TodoRepository repository;

    @PostMapping(value = "/todos/default-example-matcher")
    public ResponseEntity<?> findAllByExampleUsingDefaultExampleMatcher(@RequestBody TodoDto probe){
        // example MUST be ENTITY
        Example<TodoDto> example = Example.of(probe);

        // Default ExampleMatcher uses AND i.e if we used todo, completed field then query will be AND
        // Default is case sensitive
        List<TodoDto> todo = repository.findAll(example);
        return ok().body(todo);
    }

    @PostMapping(value = "/todos/custom-example-matcher")
    public ResponseEntity<?> findAllByExampleUsingCustomExampleMatcher(@RequestBody TodoDto exampleReq){
        // example MUST be ENTITY

        // custom ExampleMatcher - this matcher is get result as per case-insensitive and string containing for field todos
        ExampleMatcher matcher = ExampleMatcher
                .matching()
                .withMatcher("todo", ExampleMatcher.GenericPropertyMatcher.of(ExampleMatcher.StringMatcher.CONTAINING, true))
                ;

        Example<TodoDto> example = Example.of(exampleReq,matcher);

        List<TodoDto> todo = repository.findAll(example);
        return ok().body(todo);
    }
}
