package com.poc.data.mongo.controller;

import com.poc.data.mongo.model.TodoDto;
import com.poc.data.mongo.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.*;


@RestController
@RequestMapping("/crud")
public class TodoCrudController {
    @Autowired
    private TodoRepository todoRepo;

    @GetMapping("/todos")
    public ResponseEntity<?> getAllTodos(){
        List<TodoDto> todos =  todoRepo.findAll();
        if(ObjectUtils.isEmpty(todos)){
            todos = new ArrayList<>();
        }
        return ok(todos);
    }

    @GetMapping("/todos/{id}")
    public ResponseEntity<?> findById(@PathVariable String id){
        TodoDto todo =  todoRepo.findById(id)
                .orElse(new TodoDto());
        return ok().body(todo);
    }

    @PostMapping("/todos")
    public ResponseEntity<?> saveTodo(@RequestBody TodoDto request){
        request.setCreatedAt(new Date());
        request.setUpdatedAt(new Date());
        TodoDto todo =  todoRepo.save(request);
        return ok().body(todo);
    }

    /**
     * update if exists else create
     * @param id
     * @param request
     * @return
     */
    @PutMapping("/todos/{id}")
    public ResponseEntity<?> updateById(@PathVariable String id, @RequestBody TodoDto request){
        TodoDto todo=request;
        todo.setUpdatedAt(new Date());
        Optional<TodoDto> oTodo =  todoRepo.findById(id);
        if(oTodo.isPresent()){
            // updating
            todo = oTodo.get();
            todo.setTodo(request.getTodo());
            todo.setDescription(request.getDescription());
        }else{
            // creating
            todo.setCreatedAt(new Date());
        }
        todo = todoRepo.save(todo);
        return ok().body(todo);
    }

    @PatchMapping("/todos/{id}")
    public ResponseEntity<?> patchById(@PathVariable String id, @RequestBody TodoDto request){
        TodoDto todo =  todoRepo.findById(id)
                .orElseThrow(()->new RuntimeException("no records found of id "+id));
        request.setUpdatedAt(new Date());
        todo.setTodo(request.getTodo());
        todo.setDescription(request.getDescription());
        todo = todoRepo.save(todo);
        return ok().body(todo);
    }

    @DeleteMapping("/todos/{id}")
    public ResponseEntity<?> deleteById(@PathVariable String id){
        TodoDto todo =  todoRepo.findById(id)
                .orElseThrow(()->new RuntimeException("no records found of id "+id));

        todoRepo.deleteById(todo.getId());
        return ok("Deleted successfully , id "+id);
    }
}
