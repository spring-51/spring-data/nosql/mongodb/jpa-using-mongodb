package com.poc.data.mongo.controller;

import com.poc.data.mongo.enums.StatusLookupEnum;
import com.poc.data.mongo.model.QTodoDto;
import com.poc.data.mongo.model.StatusLookup;
import com.poc.data.mongo.model.TodoDto;
import com.poc.data.mongo.repository.TodoRepository;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = "/query-dsl")
@RequiredArgsConstructor
public class TodoQueryDslController {

    private final TodoRepository todoRepo;

    @GetMapping("/todos")
    public ResponseEntity<?> getAllTodos(){
        QTodoDto qTodoDto = new QTodoDto("getAllTodos") ;
        // WAY 1 - [WORKING]
        // BooleanExpression predicate = qTodoDto.status.eq(new StatusLookup());

        // [WORKING] - WAY 2
        // RECOMMENDED over WAY1 - why ? , ans - when query is formed using WAY1, query has to follow specific sequence
                                           // - refer - TodoRepository -> findAllTodos
        Predicate statusIdPredicate = qTodoDto.status.id.eq(StatusLookupEnum.ACTIVE.getId());
        Predicate statusPredicate = qTodoDto.status.status.eq(StatusLookupEnum.ACTIVE.name());
        Predicate predicate = ((BooleanExpression) statusIdPredicate).and(statusPredicate);


        List<TodoDto> todo =  (List<TodoDto>)todoRepo.findAll(predicate);
        return ok().body(todo);
    }

    @GetMapping("/todos/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") String id){
        QTodoDto qTodoDto = new QTodoDto("test") ;
        Predicate idPredicate = qTodoDto.id.eq(id);
        List<TodoDto> todo =  (List<TodoDto>)todoRepo.findAll(idPredicate);
        return ok().body(todo);
    }

    @GetMapping("/and/todos/{id}")
    public ResponseEntity<?> findByIdAndTodoStrLike(
            @PathVariable("id") String id,
            @RequestParam(name = "str", required = false, defaultValue = "todo") String str){
        QTodoDto qTodoDto = new QTodoDto("test") ;
        Predicate idPredicate = qTodoDto.id.eq(id);
        // issue with Spring data mongo
        // Predicate likePredicate  = qTodoDto.todo.likeIgnoreCase(str);
        // WORKAROUND of likeIgnoreCase
        Predicate likePredicate  = qTodoDto.todo.containsIgnoreCase(str);
        Predicate predicate = ((BooleanExpression)idPredicate).and(likePredicate);
        List<TodoDto> todo =  (List<TodoDto>)todoRepo.findAll(predicate);
        return ok().body(todo);
    }

    @GetMapping("/or/todos/{id}")
    public ResponseEntity<?> findByIdOrTodoStrLike(
            @PathVariable("id") String id,
            @RequestParam(name = "str", required = false, defaultValue = "todo") String str){
        QTodoDto qTodoDto = new QTodoDto("test") ;
        Predicate idPredicate = qTodoDto.id.eq(id);
        // issue with Spring data mongo
        // Predicate likePredicate  = qTodoDto.todo.likeIgnoreCase(str);
        // WORKAROUND of likeIgnoreCase
        Predicate likePredicate  = qTodoDto.todo.containsIgnoreCase(str);
        Predicate predicate = ((BooleanExpression)idPredicate).or(likePredicate);
        List<TodoDto> todo =  (List<TodoDto>)todoRepo.findAll(predicate);
        return ok().body(todo);
    }

    @GetMapping("/and-or/todos/{id}")
    public ResponseEntity<?> findByIdAndOrCombinationTodoStrLike(
            @PathVariable("id") String id,
            @RequestParam(name = "str", required = false, defaultValue = "todo") String str){
        QTodoDto qTodoDto = new QTodoDto("test") ;
        Predicate idPredicate = qTodoDto.id.eq(id);
        // issue with Spring data mongo
        // Predicate likePredicate  = qTodoDto.todo.likeIgnoreCase(str);
        // WORKAROUND of likeIgnoreCase
        Predicate likePredicate  = qTodoDto.todo.containsIgnoreCase(str);
        Predicate completedPredicate = qTodoDto.completed.eq(true);
        Predicate predicate = ((BooleanExpression)idPredicate).and(likePredicate).or(completedPredicate);
        List<TodoDto> todo =  (List<TodoDto>)todoRepo.findAll(predicate);
        return ok().body(todo);
    }
}
