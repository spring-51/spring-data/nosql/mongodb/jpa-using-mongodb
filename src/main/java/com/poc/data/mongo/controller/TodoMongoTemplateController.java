package com.poc.data.mongo.controller;

import com.poc.data.mongo.enums.StatusLookupEnum;
import com.poc.data.mongo.model.StatusLookup;
import com.poc.data.mongo.model.TodoDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/mongo-template")
@RequiredArgsConstructor
public class TodoMongoTemplateController {

    private final MongoTemplate mongoTemplate;

    @GetMapping("/todos")
    public ResponseEntity<?> findAll(){
        Query query =new Query();
        // WAY 1 - WORKING
        // Criteria criteria = Criteria.where("status").is(new StatusLookup());

        // WAY 2 - Working
        // RECOMMENDED over WAY1 - why ? , ans - when query is formed using WAY1, query has to follow specific sequence
                                                 // - refer - TodoRepository -> findAllTodos
        Criteria statusIdCriteria = Criteria.where("status._id").is(StatusLookupEnum.ACTIVE.getId());
        Criteria statusCriteria = Criteria.where("status.status").is(StatusLookupEnum.ACTIVE.name());
        Criteria criteria = new Criteria().andOperator(statusIdCriteria, statusCriteria);


        query.addCriteria(criteria);
        List<TodoDto> todo = mongoTemplate.find(query,TodoDto.class);
        return ok().body(todo);
    }

    @GetMapping("/and/todos/{id}")
    public ResponseEntity<?> findByIdAnd(@PathVariable("id") String id){
        Criteria criteria = new Criteria();
        Criteria hasStatusId = Criteria.where("statusId").is(1L);
        Criteria hasId = Criteria.where("id").is(id);
        criteria.andOperator(hasStatusId,hasId);

        Query query =new Query(criteria);
        List<TodoDto> todo = mongoTemplate.find(query,TodoDto.class);
        return ok().body(todo);
    }

    @GetMapping("/or/todos/{id}")
    public ResponseEntity<?> findByIdOr(@PathVariable("id") String id){
        Criteria criteria = new Criteria();
        Criteria hasStatusId = Criteria.where("statusId").is(1L);
        Criteria hasId = Criteria.where("id").is(id);
        criteria.orOperator(hasStatusId,hasId);
        Query query =new Query(criteria);
        List<TodoDto> todo = mongoTemplate.find(query,TodoDto.class);
        return ok().body(todo);
    }

    @GetMapping("/and-or/todos")
    public ResponseEntity<?> findByIdAndOrCombination(
            @RequestParam(name = "ids", required = false, defaultValue = "a,b") List<String> ids){
        Criteria criteria = new Criteria();
        Criteria hasStatusId = Criteria.where("statusId").is(1L);
        List<Criteria> orCriteria = ids.stream().map(e->Criteria.where("id").is(e)).collect(Collectors.toList());
        criteria.andOperator(hasStatusId).orOperator(orCriteria.toArray(new Criteria[]{}));
        Query query =new Query(criteria);
        List<TodoDto> todo = mongoTemplate.find(query,TodoDto.class);
        return ok().body(todo);
    }
}
