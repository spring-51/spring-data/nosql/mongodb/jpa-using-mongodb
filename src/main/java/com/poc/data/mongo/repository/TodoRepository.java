package com.poc.data.mongo.repository;

import com.poc.data.mongo.model.TodoDto;
import org.omg.PortableInterceptor.ACTIVE;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TodoRepository extends MongoRepository<TodoDto, String>,
        QuerydslPredicateExecutor<TodoDto> // for enabling Query DSL
{
    // WAY 1
    // [WORKING] - ONLY IF  query formed as { "status" : { "_id" : 1, "status" : "ACTIVE"}}
                  // if query formed as { "status" : {"status" : "ACTIVE", "_id" : 1}} - IT WILL NOT WORK , why ?
    // @Query(value = "{'status':{'_id': ?1,'status': ?0}}") // here 0,1 means first, second method args
    // WAY 2 - Recommended over WAY1
    @Query(value = "{'status.status' : ?0, 'status._id' : ?1}") // here 0,1 means first, second method args
    List<TodoDto> findAllTodos(String status, Long id);
    @Query(value = "{id : ?0}") // here 0 means first method args
    Optional<TodoDto> findTodoById(String id);
    @Query(value = "{todo : {$regex : ?0}}") // here 0 means first method args
    Optional<List<TodoDto>> findTodoByLikeTodoStrCaseSensitive(String todoStr);
    // refer - https://www.tutorialspoint.com/mongodb-regex-search-on-integer-value
    @Query(value = "{ $where: '/?1.*/.test(this.?0)' }") // here 0 means first method args
    Optional<List<TodoDto>> findTodoByLikeTodoStrAndIntegerCaseSensitive(String fieldName, String searchStr);
    @Query(value = "{todo : {$regex : ?0, $options: 'i'}}") // here 0 means first method args
    Optional<List<TodoDto>> findTodoByLikeTodoStrCaseInsensitive(String todoStr);
    @Query(value = "{id : { $in: ?0 }}") // here 0 means first method args
    Optional<List<TodoDto>> findTodoByInIds(List<String> ids);
}
