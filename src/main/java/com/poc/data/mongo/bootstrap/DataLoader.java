package com.poc.data.mongo.bootstrap;

import com.poc.data.mongo.model.TodoDto;
import com.poc.data.mongo.repository.TodoRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;

@Component
public class DataLoader  implements CommandLineRunner {

    private final TodoRepository repository;

    public DataLoader(TodoRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... args) {
        TodoDto todo1 = TodoDto.builder().todo("test-todo-1001").todoId(100).description("test-todo-1001-desc").completed(false).createdAt(new Date()).updatedAt(new Date()).build();
        TodoDto todo2 = TodoDto.builder().todo("test-todo-1002").todoId(2001).description("test-todo-1002-desc").completed(true).createdAt(new Date()).updatedAt(new Date()).build();
        TodoDto todo3 = TodoDto.builder().todo("test-todo-2001").todoId(3002).description("test-todo-2001-desc").completed(false).createdAt(new Date()).updatedAt(new Date()).build();
        TodoDto todo4 = TodoDto.builder().todo("test-todo-2002").todoId(4003).description("test-todo-2002-desc").completed(true).createdAt(new Date()).updatedAt(new Date()).build();
        repository.insert(Arrays.asList(todo1, todo2, todo3, todo4));
    }
}
