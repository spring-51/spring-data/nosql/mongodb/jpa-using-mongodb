package com.poc.data.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;

import javax.validation.Validator;

@SpringBootApplication
public class JpaUsingMongodbApplication {
	@Autowired
	private Validator validator;

	public static void main(String[] args) {
		SpringApplication.run(JpaUsingMongodbApplication.class, args);
	}

	@Bean
	public ValidatingMongoEventListener validatingMongoEventListener() {
		return new ValidatingMongoEventListener(validator);
	}
}
