package com.poc.data.mongo.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Setter @Getter @NoArgsConstructor @AllArgsConstructor @Builder
@Document(collection = "todos") // similar to @Entity(name="todos")
public class TodoDto {

    @Id
    private String id;
    @NotNull(message = "field '%s' can't be null")
    private String todo;
    @NotNull(message = "field '%s' can't be null")
    private String description;
    private Boolean completed;
    private Date createdAt;
    private Date updatedAt;
    @Builder.Default
    private Long  statusId = 1L;
    @Builder.Default
    private StatusLookup status = new StatusLookup();
    private Integer todoId;

}
