package com.poc.data.mongo.model;

import com.poc.data.mongo.enums.StatusLookupEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class StatusLookup {
    private Long id = StatusLookupEnum.ACTIVE.getId();
    private String status =StatusLookupEnum.ACTIVE.name();

    public StatusLookup(Long id) {
        this.id = id;
    }
}
