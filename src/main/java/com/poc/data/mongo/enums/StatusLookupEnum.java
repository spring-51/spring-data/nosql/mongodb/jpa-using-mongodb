package com.poc.data.mongo.enums;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public enum StatusLookupEnum {
    ACTIVE(1l);
    @Getter
    private Long id;

    StatusLookupEnum(Long id) {
        this.id = id;
    }

}
