# jpa-using-mongodb

### 1. PREQUISITE
```
1.1 : mongodb( OPTIONAL)
  - if using embedded mongodb
  <dependency>
    <groupId>de.flapdoodle.embed</groupId>
    <artifactId>de.flapdoodle.embed.mongo</artifactId>
  </dependency>
```

### 2. Good to have
```
2.1. Robo 3t
Ques - how can we check data for embedded db
```

### IMP THINGS
```
- when we add only  spring.data.mongodb.uri=mongodb://127.0.0.1:27017/todo-manage-api
in application properties file. It will not create db until we insert one document into
a collection
```

### 1. CRUD
```
- refer - TodoCrudController
```
### 2. Get/Read

#### 2.1 using @Query
```
- refer - TodoQueryController

NOTE : 
When we use contains for number field we are currently using $where
refer 
- TodoQueryController -> findTodoByLikeTodoStrCaseSensitive
- refer - https://www.tutorialspoint.com/mongodb-regex-search-on-integer-value
- problem - this is working for string seach but ONLY CASE SENSITIVE SEARCH
  -- how can we make it case-in-sensitive ?
  -- do we have any better approach to solve contains in number field in mongo ?
    --- refer - https://stackoverflow.com/questions/24222428/sql-like-operations-on-numbers-in-mongodb 
       ---- it talks about contains in number is not good practice
```

#### 2.2 using Criteria API [MongoTemplate]

##### and/or 
```
- refer - TodoMongoTemplateController
```

#### 2.3 using Example/Executor
```
- refer - TodoExampleExecutorController
```

#### 2.4 using Query Dsl
```
Steps to configure Query DSL
 s1 - add below dependency(refer pom.xml)

        <dependency>
			<groupId>com.querydsl</groupId>
			<artifactId>querydsl-mongodb</artifactId>
			<version>4.4.0</version>
		</dependency>
		<dependency>
			<groupId>com.querydsl</groupId>
			<artifactId>querydsl-apt</artifactId>
			<version>4.4.0</version>
		</dependency>

  s2  - add below plugin(refer pom.xml)
        <plugin>
  				<groupId>com.mysema.maven</groupId>
  				<artifactId>apt-maven-plugin</artifactId>
  				<version>1.1.3</version>
  				<executions>
  					<execution>
  						<goals>
  							<goal>process</goal>
  						</goals>
  						<configuration>
  							<outputDirectory>target/generated-sources/java</outputDirectory>
  							<processor>
  								org.springframework.data.mongodb.repository.support.MongoAnnotationProcessor
  							</processor>
  						</configuration>
  					</execution>
  				</executions>
  			</plugin>

 S3 -
 Extends your repo by  QuerydslPredicateExecutor(eg. ToDoRepository)

 s4 - mvn clean install
      -- it will auto generate QXxx classes
 s5 - write implementation using Query dsl
 s6 - if Qxxx is not able to resolve , go to maven tab of intelliJ and reload project.

```
```
- refer - TodoQueryDslController
```






###  Annotation
```
1. @Document 
- it's same as Entity for RDBMS
  -- refer TodoDto
2. @Query - it comes in package of mongo, its same as @Query of jpa, only here query syntax is diff
```

### References

```
Mongo db
 - https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#reference

Criteria API
 - https://www.baeldung.com/queries-in-spring-data-mongodb

@Query
 - https://www.concretepage.com/spring-5/spring-data-mongodb-query

```